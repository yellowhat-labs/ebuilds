EAPI=8

inherit cargo git-r3

DESCRIPTION="A custom statusbar, written in rust"
HOMEPAGE="https://gitlab.com/yellowhat-labs/statusbar"
EGIT_REPO_URI="https://gitlab.com/yellowhat-labs/${PN}"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	git-r3_src_unpack
	cargo_live_src_unpack
}

src_install() {
	cargo_src_install
}
